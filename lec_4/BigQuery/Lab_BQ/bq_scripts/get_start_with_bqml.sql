#####################################################################################################################
-- Create a dataset

-- To create a dataset, highlight your project ID and then select CREATE DATASET.

-- Next, name your dataset bqml_lab and click Create dataset.

-- Create a model

-- Now, move on to your task!

-- Type or paste the following query to create a model that predicts whether a visitor will make a transaction:

#standardSQL
CREATE OR REPLACE MODEL `bqml_lab.sample_model`
OPTIONS(model_type='logistic_reg') AS
SELECT
  IF(totals.transactions IS NULL, 0, 1) AS label,
  IFNULL(device.operatingSystem, "") AS os,
  device.isMobile AS is_mobile,
  IFNULL(geoNetwork.country, "") AS country,
  IFNULL(totals.pageviews, 0) AS pageviews
FROM
  `bigquery-public-data.google_analytics_sample.ga_sessions_*`
WHERE
  _TABLE_SUFFIX BETWEEN '20160801' AND '20170631'
LIMIT 100000;

-- Here the visitor's device's operating system is used, whether said device is a mobile device, the visitor's 
-- country and the number of page views as the criteria for whether a transaction has been made.

-- In this case, bqml_lab is the name of the dataset and sample_model is the name of the model. The model type 
-- specified is binary logistic regression. In this case, label is what you're trying to fit to.

-- Note: If you're only interested in 1 column, this is an alternative way to setting input_label_cols.
-- The training data is being limited to those collected from 1 August 2016 to 30 June 2017. This is done to 
-- save the last month of data for "prediction". It is further limited to 100,000 data points to save some time.

-- Running the CREATE MODEL command creates a Query Job that will run asynchronously so you can, for example, 
-- close or refresh the BigQuery UI window.

#####################################################################################################################

-- (Optional) Model information & training statistics

-- If interested, you can get information about the model by clicking on bqml_lab then click the sample_model dataset 
-- in the UI. Under the Details tab you should find some basic model info and training options used to produce the 
-- model. 

#####################################################################################################################

-- Evaluate the model

-- Now replace the query with the following:

#standardSQL
SELECT
  *
FROM
  ml.EVALUATE(MODEL `bqml_lab.sample_model`, (
SELECT
  IF(totals.transactions IS NULL, 0, 1) AS label,
  IFNULL(device.operatingSystem, "") AS os,
  device.isMobile AS is_mobile,
  IFNULL(geoNetwork.country, "") AS country,
  IFNULL(totals.pageviews, 0) AS pageviews
FROM
  `bigquery-public-data.google_analytics_sample.ga_sessions_*`
WHERE
  _TABLE_SUFFIX BETWEEN '20170701' AND '20170801'));


-- If used with a linear regression model, the above query returns the following columns:

-- * mean_absolute_error, mean_squared_error, mean_squared_log_error,
-- * median_absolute_error, r2_score, explained_variance.

-- If used with a logistic regression model, the above query returns the following columns:

-- * precision, recall
-- * accuracy, f1_score
-- * log_loss, roc_auc

-- Please consult the machine learning glossary or run a Google search to understand how each of these metrics are 
-- calculated and what they mean.

-- You'll realize the SELECT and FROM portions of the query is identical to that used during training. The WHERE 
-- portion reflects the change in time frame and the FROM portion shows that you're calling ml.EVALUATE.



#####################################################################################################################

-- Use the Model

-- Predict purchases per country

-- With this query you will try to predict the number of transactions made by visitors of each country, 
-- sort the results, and select the top 10 countries by purchases:

#standardSQL
SELECT
  country,
  SUM(predicted_label) as total_predicted_purchases
FROM
  ml.PREDICT(MODEL `bqml_lab.sample_model`, (
SELECT
  IFNULL(device.operatingSystem, "") AS os,
  device.isMobile AS is_mobile,
  IFNULL(totals.pageviews, 0) AS pageviews,
  IFNULL(geoNetwork.country, "") AS country
FROM
  `bigquery-public-data.google_analytics_sample.ga_sessions_*`
WHERE
  _TABLE_SUFFIX BETWEEN '20170701' AND '20170801'))
GROUP BY country
ORDER BY total_predicted_purchases DESC
LIMIT 10;


-- This query is very similar to the evaluation query demonstrated in the previous section. Instead of ml.EVALUATE, 
-- you're using ml.PREDICT and the BQML portion of the query is wrapped with standard SQL commands. For this lab 
-- you''re interested in the country and the sum of purchases for each country, so that's why SELECT, GROUP BY and 
-- ORDER BY. LIMIT is used to ensure you only get the top 10 results.

#####################################################################################################################

-- Predict purchases per user

-- Here is another example. This time you will try to predict the number of transactions each visitor makes, 
-- sort the results, and select the top 10 visitors by transactions:

#standardSQL
SELECT
  fullVisitorId,
  SUM(predicted_label) as total_predicted_purchases
FROM
  ml.PREDICT(MODEL `bqml_lab.sample_model`, (
SELECT
  IFNULL(device.operatingSystem, "") AS os,
  device.isMobile AS is_mobile,
  IFNULL(totals.pageviews, 0) AS pageviews,
  IFNULL(geoNetwork.country, "") AS country,
  fullVisitorId
FROM
  `bigquery-public-data.google_analytics_sample.ga_sessions_*`
WHERE
  _TABLE_SUFFIX BETWEEN '20170701' AND '20170801'))
GROUP BY fullVisitorId
ORDER BY total_predicted_purchases DESC
LIMIT 10;

#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
#####################################################################################################################