use bts;
drop table if exists flights;
CREATE OR REPLACE VIEW flights
-- CREATE MATERIALIZED VIEW flights_view
-- PARTITION BY DATE_TRUNC(FL_DATE, MONTH)
AS
SELECT
  FlightDate AS FL_DATE,
  Reporting_Airline AS UNIQUE_CARRIER,
  OriginAirportSeqID AS ORIGIN_AIRPORT_SEQ_ID,
  Origin AS ORIGIN,
  DestAirportSeqID AS DEST_AIRPORT_SEQ_ID,
  Dest AS DEST,
  CRSDepTime AS CRS_DEP_TIME,
  DepTime AS DEP_TIME,
  CAST(DepDelay AS DECIMAL(10,4)) AS DEP_DELAY,
  CAST(TaxiOut AS DECIMAL(10,4)) AS TAXI_OUT,
  WheelsOff AS WHEELS_OFF,
  WheelsOn AS WHEELS_ON,
  CAST(TaxiIn AS DECIMAL(10,4)) AS TAXI_IN,
  CRSArrTime AS CRS_ARR_TIME,
  ArrTime AS ARR_TIME,
  CAST(ArrDelay AS DECIMAL(10,4)) AS ARR_DELAY,
  IF(Cancelled = '1.00', True, False) AS CANCELLED,
  IF(Diverted = '1.00', True, False) AS DIVERTED,
  DISTANCE
FROM flights_raw;

CREATE OR REPLACE VIEW delayed_10 AS
SELECT * FROM flights WHERE dep_delay >= 10;

CREATE OR REPLACE VIEW delayed_15 AS
SELECT * FROM flights WHERE dep_delay >= 15;

CREATE OR REPLACE VIEW delayed_20 AS
SELECT * FROM flights WHERE dep_delay >= 20;

-- CREATE VIEW delayed_10 AS SELECT * FROM flights_view WHERE dep_delay > 10;
-- CREATE VIEW delayed_15 AS SELECT * FROM flights_view WHERE dep_delay > 15;
-- CREATE VIEW delayed_20 AS SELECT * FROM flights_view WHERE dep_delay > 20;

