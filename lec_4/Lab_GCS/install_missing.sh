#!/bin/bash

sudo apt-get update
sudo apt-get -y -qq --fix-missing install python-mpltoolkits.basemap python-numpy python-matplotlib

pip install -U matplotlib==3.2