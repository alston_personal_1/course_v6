#!/usr/bin/env bash

today=$(date '+%Y%m%d')
logfile="/home/alstonyan/projects/Bing_InsertionOrder/V_1/Log/Bing_InsertionOrder_V1-$today.txt"

python /home/alstonyan/projects/Bing_InsertionOrder/V_1/Code/Bing_InsertionOrder_V1.py >> $logfile 2>&1 \
&& python /home/alstonyan/projects/Bing_InsertionOrder/V_1/Code/Bing_GetAllAcctNo.py >> $logfile 2>&1