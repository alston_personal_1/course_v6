#!/usr/bin/python

from Bing_GetCusService import get_service

def get_all_accounts():
    InputServiceType = 'CustomerManagementService'
    customer_service = get_service(InputServiceType)

    get_user_response=customer_service.GetUser(UserId=None)
    user = get_user_response.User
    user_id = user.Id

    i = 0
    AccountIds = []

    paging={
        'Index': i,
        'Size': 100
    }

    predicates={
        'Predicate': [
            {
                'Field': 'UserId',
                'Operator': 'Equals',
                'Value': user_id,
            },
        ]
    }

    search_accounts_request={
        'PageInfo': paging,
        'Predicates': predicates
    }
        
    accounts = customer_service.SearchAccounts(PageInfo=paging,Predicates=predicates)
    for account in accounts['Account']:
        AccountIds.append(str(account['Id']))
    

    while 'Account' in dir(accounts):
        try:
            print("Page: {}".format(i))
            i += 1
            paging={
                'Index': i,
                'Size': 100
            }
            accounts = customer_service.SearchAccounts(PageInfo=paging,Predicates=predicates)
            for account in accounts['Account']:
                AccountIds.append(str(account['Id']))
        except TypeError:
            print("No more pages")

    print("Got Total {} Account Ids for User {}".format(len(AccountIds), user.UserName))

    return AccountIds
