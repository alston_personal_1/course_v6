#!/usr/bin/python

import sys
import webbrowser
from time import gmtime, strftime
from suds import WebFault

from bingads.service_client import ServiceClient
from bingads.authorization import *
from bingads.v11 import *
from bingads.v11.bulk import *
from bingads.v11.reporting import *

def get_service(InputServiceType):
    DEVELOPER_TOKEN='1334B332R0xxxxxx'
    ENVIRONMENT='production'

    CLIENT_ID='28a65e3d-xxxx-xxxx-xxxx-8d9dc7c79476'
    CLIENT_STATE='ClientStateGoesHere'
    REFRESH_TOKEN_PATH="/home/alstonyan/credentials/refresh.txt"

    def authenticate_with_oauth(authorization_data):
    
        authentication=OAuthDesktopMobileAuthCodeGrant(
            client_id=CLIENT_ID
        )

        authentication.state=CLIENT_STATE

        authorization_data.authentication=authentication   

        authorization_data.authentication.token_refreshed_callback=save_refresh_token
    
        refresh_token=get_refresh_token()
    
        try:
            if refresh_token is not None:
                authorization_data.authentication.request_oauth_tokens_by_refresh_token(refresh_token)
            else:
                request_user_consent(authorization_data)
        except OAuthTokenRequestException:
            request_user_consent(authorization_data)
        
    def get_refresh_token():
        file=None
        try:
            file=open(REFRESH_TOKEN_PATH)
            line=file.readline()
            file.close()
            return line if line else None
        except IOError:
            if file:
                file.close()
            return None

    def save_refresh_token(oauth_tokens):
        with open(REFRESH_TOKEN_PATH,"w+") as file:
            file.write(oauth_tokens.refresh_token)
            file.close()
        return None


    authorization_data = AuthorizationData(
        account_id=None,
        customer_id=None,
        developer_token=DEVELOPER_TOKEN,
        authentication=None,
    )
    
    customer_service=ServiceClient(
        InputServiceType, 
        authorization_data=authorization_data, 
        environment=ENVIRONMENT,
        version=11,)
    
    authenticate_with_oauth(authorization_data)
    
    return customer_service

