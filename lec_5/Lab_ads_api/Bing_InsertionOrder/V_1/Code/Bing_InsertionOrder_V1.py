#!/usr/bin/python


from Bing_GetCusService import get_service
from Bing_GetAllAccounts import get_all_accounts

import pandas as pd
import time
from datetime import timedelta, date, datetime

import os
import tempfile
from sqlalchemy import create_engine

import boto
import gcs_oauth2_boto_plugin
import yaml
import shutil


def extract_bing_CBS_data_1stId(Account_Id_1st, customer_service_CBS):
    i = 0
    insertionOrder_1stpage = get_one_page_data(i, Account_Id_1st, customer_service_CBS)

    # Extract Columns and Create a New PANDAS Dataframe
    columnNames = insertionOrder_1stpage.InsertionOrder[0].__keylist__
    df_raw = pd.DataFrame(columns=columnNames)

    # Put First Page's data into dataframe
    df_raw = dataframe_append_rows(insertionOrder_1stpage, df_raw, columnNames)

    # Detect next page 
    while 'InsertionOrder' in dir(insertionOrder_1stpage):
        try:
            print("Already extracting data from Page: {}, continue for next page".format(i))
            i += 1
            insertionOrder_1stpage = get_one_page_data(i, Account_Id_1st, customer_service_CBS)
            df_raw = dataframe_append_rows(insertionOrder_1stpage, df_raw, columnNames)

        except AttributeError:
            print("No more pages")
    print("For Account_Id: {}, Data Extracting process is done!".format(Account_Id_1st))

    return df_raw


def extract_bing_CBS_data_rest(df_raw, rest_accountId_n, customer_service_CBS):
    i = 0
    insertionOrder_1page = get_one_page_data(i, rest_accountId_n, customer_service_CBS)

    # Extract Columns

    # columnNames = insertionOrder_1page.InsertionOrder[0].__keylist__
    columnNames = ['AccountId', 'BalanceAmount', 'BookingCountryCode', 'Comment', 'EndDate', 'InsertionOrderId',
                   'LastModifiedByUserId', 'LastModifiedTime', 'NotificationThreshold', 'ReferenceId', 'SpendCapAmount',
                   'StartDate', 'Name', 'Status', 'PurchaseOrder', 'ChangePendingReview']

    df_raw = dataframe_append_rows(insertionOrder_1page, df_raw, columnNames)

    # Detect next page 
    while 'InsertionOrder' in dir(insertionOrder_1page):
        try:
            print("Already extracting data from Page: {}, continue for next page".format(i))
            i += 1
            insertionOrder_1page = get_one_page_data(i, rest_accountId_n, customer_service_CBS)

            df_raw = dataframe_append_rows(insertionOrder_1page, df_raw, columnNames)

        except AttributeError:
            print("No more pages")

    print("For Account_Id: {}, Data Extracting process is done!".format(rest_accountId_n))

    return df_raw


def dataframe_append_rows(insertionOrder_RAW, df_raw, columnNames):
    for items in insertionOrder_RAW.InsertionOrder:
        row_n = []
        for columnName in columnNames:
            row_n.append(items[columnName])

        df_raw.loc[-1] = row_n
        df_raw.index = df_raw.index + 1
        df_raw = df_raw.sort_index()

    return df_raw


def get_one_page_data(i, Account_Id_n, customer_service_CBS):
    paging_1 = {
        'Index': i,
        'Size': 100
    }
    predicates = {
        'Predicate': [
            {
                'Field': 'AccountId',
                'Operator': 'Equals',
                'Value': Account_Id_n,
            },
        ]
    }
    while True:
        try:
            insertionOrder_1page = customer_service_CBS.SearchInsertionOrders(
                PageInfo=paging_1,
                Predicates=predicates)
        except:
            print("Retry in 5s")
            time.sleep(5)
            continue
        break
    return insertionOrder_1page


def clean_data(df_raw):
    print("-------Cleaning Data-------")

    print("-------Reformat EndDate-------")
    df_raw['EndDate'] = df_raw['EndDate'].astype(str).apply(lambda x: x.split(' ')[0]).apply(
        lambda x: datetime.strptime(x, '%Y-%m-%d'))

    print("-------Reformat LastModifiedTime-------")
    df_raw['LastModifiedTime'] = df_raw['LastModifiedTime'].astype(str).apply(lambda x: x.split(' ')[0]).apply(
        lambda x: datetime.strptime(x, '%Y-%m-%d'))

    print("--------------Delete the duplicates--------------")
    df_raw_final = df_raw.drop_duplicates(keep='first')

    print("@@@@@@@@@@@@@@ Convert UTF-8 @@@@@@@@@@@@@@@@@@@@@")
    yesterday = date.today() - timedelta(1)
    dt = yesterday.strftime("%Y-%m-%d")

    temp_dir = tempfile.mkdtemp(prefix='googlestorage')
    final_filename = "BING_INSERTION_ORDER_for_{}.csv".format(dt)
    BingInsertionOrder_path = os.path.join(temp_dir, final_filename)
    print("@@@@@@@@@@@@@@ Create a new temp dir at {} @@@@@@@@@@@@@@@@@@@@@".format(BingInsertionOrder_path))
    df_raw_final.to_csv(BingInsertionOrder_path, index=False, encoding='utf-8')
    time.sleep(5)

    print("@@@@@@@@@@@@@@ Read from CSV @@@@@@@@@@@@@@@@@@@@")
    df_raw_final_ut8 = pd.read_csv(BingInsertionOrder_path, header=0)

    print("@@@@@@@@@@@@@@ Load to SQL @@@@@@@@@@@@@@@@@@@@@")
    TableName_InsertionOrder = 'Bing_InsertionOrder_Raw'
    engine = create_engine("mysql+mysqldb://root:" + 'IloveData' + "@xx.xx.xx.xx/Bing_InsertionOrder?charset=utf8",
                           encoding="utf-8")

    df_raw_final_ut8.to_sql(con=engine, name=TableName_InsertionOrder, if_exists='replace', index=False)

    LoadToGS(temp_dir, final_filename)


def LoadToGS(temp_dir, final_filename):
    # Get credentials from gcs
    GOOGLE_STORAGE = 'gs'
    conf = yaml.load(open('/home/alstonyan/credentials/gcs.yaml'))
    CLIENT_ID = conf['gcs']['client_id']
    CLIENT_SECRET = conf['gcs']['client_secret']
    gcs_oauth2_boto_plugin.SetFallbackClientIdAndSecret(CLIENT_ID, CLIENT_SECRET)

    print('Uploading data into google cloud storage')
    with open(os.path.join(temp_dir, final_filename), 'r') as localfile:
        dst_uri = boto.storage_uri('xxxxx/Bing_InsertionOrder/V_1/' + final_filename, GOOGLE_STORAGE)
        dst_uri.new_key().set_contents_from_file(localfile)
        print ('Successfully created "{}/{}"'.format(dst_uri.bucket_name, dst_uri.object_name))
    shutil.rmtree(temp_dir)


def main():
    AccountIds = get_all_accounts()

    InputServiceType_CBS = 'CustomerBillingService'
    customer_service_CBS = get_service(InputServiceType_CBS)

    Account_Id_1st = AccountIds[0]
    df_raw = extract_bing_CBS_data_1stId(Account_Id_1st, customer_service_CBS)

    for rest_accountId_n in AccountIds[1:]:
        try:
            df_raw = extract_bing_CBS_data_rest(df_raw, rest_accountId_n, customer_service_CBS)
        except  AttributeError:
            print("-----Empty Attribute in account {}-----".format(rest_accountId_n))
            pass

    clean_data(df_raw)


if __name__ == '__main__':
    main()
