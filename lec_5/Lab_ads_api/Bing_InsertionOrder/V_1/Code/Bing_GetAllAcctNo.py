
import os
import tempfile
from sqlalchemy import create_engine
import pandas as pd
from Bing_GetCusService import get_service

def get_all_accounts():
    InputServiceType = 'CustomerManagementService'
    customer_service = get_service(InputServiceType)

    get_user_response=customer_service.GetUser(UserId=None)
    user = get_user_response.User
    user_id = user.Id

    i = 0
    AccountIds = []
    AccountNos = []

    paging={
        'Index': i,
        'Size': 100
    }

    predicates={
        'Predicate': [
            {
                'Field': 'UserId',
                'Operator': 'Equals',
                'Value': user_id,
            },
        ]
    }

    search_accounts_request={
        'PageInfo': paging,
        'Predicates': predicates
    }
        
    accounts = customer_service.SearchAccounts(PageInfo=paging,Predicates=predicates)
    for account in accounts['Account']:
        AccountIds.append(str(account['Id']))
        AccountNos.append(str(account['Number']))
    

    while 'Account' in dir(accounts):
        try:
            print("Page: {}".format(i))
            i += 1
            paging={
                'Index': i,
                'Size': 100
            }
            accounts = customer_service.SearchAccounts(PageInfo=paging,Predicates=predicates)
            for account in accounts['Account']:
                AccountIds.append(str(account['Id']))
                AccountNos.append(str(account['Number']))
        except TypeError:
            print("No more pages")

    print("Got Total {} Account Ids for User {}".format(len(AccountIds), user.UserName))

    AccountInfo = zip(AccountIds, AccountNos)

    return AccountInfo


def main():
    AccountInfo = get_all_accounts()

    df_AcctInfo = pd.DataFrame.from_dict(AccountInfo,orient='columns')
    df_AcctInfo.columns = ['AccountId','AccountNo']

    print("@@@@@@@@@@@@@@ Load to SQL @@@@@@@@@@@@@@@@@@@@@")

    TableName_AccountInfo = 'Bing_AccountInfo'
    engine = create_engine("mysql+mysqldb://root:" + 'IloveData' + "@xx.xx.xx.xx/Bing_InsertionOrder?charset=utf8",
        encoding="utf-8")

    df_AcctInfo.to_sql(con=engine, name=TableName_AccountInfo, if_exists='replace', index=False)

    print("@@@@@@@@@@@@@@ Done @@@@@@@@@@@@@@@@@@@@@")


if __name__ == '__main__':
    main()


















