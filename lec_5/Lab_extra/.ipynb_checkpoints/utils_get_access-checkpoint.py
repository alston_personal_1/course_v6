import yaml
import json


class get_access(object):
    def __init__(self, info_path):
        config = json.load(open(info_path, 'r'))

        print('Found info file in path {}'.format(info_path))
        key_file = yaml.load(open(config['credential_path']['outlook'], 'r'), Loader=yaml.BaseLoader)
        self.SERVER = key_file['outlook_login']['server']
        self.EMAIL = key_file['outlook_login']['email']
        self.USERNAME = key_file['outlook_login']['username']
        self.PASSWORD = key_file['outlook_login']['password']


if __name__ == "__main__":
    info_path = "./info.json"

    ed = get_access(info_path)
    print(ed.SERVER)