import os
import sys

current_workdir = os.path.abspath(os.path.join(os.getcwd(), ".."))
sys.path.append(os.path.abspath(os.path.join(os.getcwd(), "..")))

from utils_get_access import get_access
from exchangelib import Account, Configuration, Credentials, DELEGATE, EWSTimeZone, UTC_NOW
from datetime import datetime, timedelta
from pytz import timezone
import re
import time


class check_email(object):
    def __init__(self, subject__contains):
        _INFO_PATH = "./info.json"
        self._EMAIL_INFO = get_access(_INFO_PATH)
        self._SERVER = self._EMAIL_INFO.SERVER
        self._EMAIL = self._EMAIL_INFO.EMAIL
        self._USERNAME = self._EMAIL_INFO.USERNAME
        self._PASSWORD = self._EMAIL_INFO.PASSWORD
        self.ACCOUNT = self.connect(self._SERVER, self._EMAIL, self._USERNAME, self._PASSWORD)
        self.SUBJECT__CONTAINS = subject__contains
        self._EMAIL_DETECT = False

    def connect(self, server, email, username, password):
        """
            Get Exchange account cconnection with server
            """
        creds = Credentials(username=username, password=password)
        config = Configuration(server=server, credentials=creds)
        return Account(primary_smtp_address=email, autodiscover=False, config=config, access_type=DELEGATE)

    def get_esttime(self):
        now = UTC_NOW()
        print("Time Now : {}".format(now))
        hour_diff = now.astimezone(timezone('America/Toronto')).hour
        minute_diff = now.astimezone(timezone('America/Toronto')).minute
        second_diff = now.astimezone(timezone('America/Toronto')).second
        email_since = now.astimezone(timezone('America/Toronto')) - timedelta(hours=hour_diff, minutes=minute_diff,
                                                                              seconds=second_diff)
        # Check 1 day's record
        return email_since

    def check_email_main(self):
        ACCOUNT = self.connect(self._SERVER, self._EMAIL, self._USERNAME, self._PASSWORD)
        EMAIL_SINCE = self.get_esttime()
        print("---> Checking time: {}".format(EMAIL_SINCE))
        for msg in ACCOUNT.inbox.filter(datetime_received__gt=EMAIL_SINCE,  # is_read=False,
                                        subject__contains=self.SUBJECT__CONTAINS) \
                           .order_by('datetime_received')[:1]:
            print(msg)
            subj = 'New mail: %s' % msg.subject
            # clean_body = '\n'.join(l for l in msg.text_body.split('\n') if l)
            print("Found a email has attachemnt in past 1 day on:{}".format(msg.datetime_received))
            print(msg.attachments)
            return msg

    def check_status(self):
        msg = self.check_email_main()
        while msg is None:
            print("---> No Email Detect")
            msg = self.check_email_main()
            print("---> Sleep for 30m")
            time.sleep(600)
        else:
            print("---> Email Catched")
            # print(clean_body)
            return msg

    def get_campcd(self, clean_body):
        try:
            camp_cd = re.search("CAMP_CD =  ‘(.+?)’.", clean_body).group(1)
        except AttributeError:
            camp_cd = ''

        return camp_cd

    def get_attachment(self, msg, download_folder, attachment_filename):
        download_path = os.path.join(download_folder, attachment_filename)
        try:
            print("Looking for file to download...")
            for attachment in msg.attachments:
                print(attachment.name)
                if attachment.name == attachment_filename:
                    print("Found attachment file !")
                    with open(download_path, 'wb') as f:
                        f.write(attachment.content)
                    print('Done!')
                else:
                    print("Error: Didn't Find attachment file !")
                    pass
        except AttributeError:
            print("Error")


def main():
    subject__contains = 'FILE'
    download_folder = './download_file'
    attachment_filename = 'csv_file.csv'
    demo = check_email(subject__contains)
    print(demo.ACCOUNT)
    msg = demo.check_status()
    demo.get_attachment(msg, download_folder, attachment_filename)


if __name__ == "__main__":
    main()
