def func_1_e():
    print("this is func_1")
    data = [1, 2, 3]
    return data


def func_2_t(data):
    data.append(4)
    return data


def func_3_l():
    pass


if __name__ == "__main__":
    data = func_1_e()
    print(data)
    new_data = func_2_t(data)
    print(new_data)
